<?php

	require_once('/home/c/cveti63ru/new.stroykomfort63.ru/public_html/api/Simpla.php');

	$simpla = new Simpla();

	$xml = new DOMDocument();
	$url = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req=' . date('d.m.Y');
	echo $url;

	if (@$xml->load($url))
	{
		$list = array(); 

		$root = $xml->documentElement;
		$items = $root->getElementsByTagName('Valute');

		foreach ($items as $item)
		{
			$code = $item->getElementsByTagName('CharCode')->item(0)->nodeValue;
			$curs = $item->getElementsByTagName('Value')->item(0)->nodeValue;
			$list[$code] = floatval(str_replace(',', '.', $curs));
		}
	}

	
	

	$cur="USD";
	$usd = $simpla->money->get_currency($cur);
	$CurentCursUSD=isset($list[$cur]) ? $list[$cur] : 0;
	$CurentCursUSD=ceil($CurentCursUSD);

	if($CurentCursUSD > 0)
	{
		$usd->rate_to = $CurentCursUSD;
		$simpla->money->update_currency($usd->id, $usd);
		$simpla->db->query("UPDATE __variants SET price=base_price*?, compare_price=base_compare_price*? WHERE currency=?", $usd->rate_to / $usd->rate_from, $usd->rate_to / $usd->rate_from, $usd->id);
	}
	


	$cur="EUR";
	$eur = $simpla->money->get_currency($cur);
	$CurentCursEUR=isset($list[$cur]) ? $list[$cur] : 0;
	$CurentCursEUR=ceil($CurentCursEUR);
	
	if($CurentCursEUR > 0)
	{
		$eur->rate_to = $CurentCursEUR;
		$simpla->money->update_currency($eur->id, $eur);
		$simpla->db->query("UPDATE __variants SET price=base_price*?, compare_price=base_compare_price*? WHERE currency=?", $eur->rate_to / $eur->rate_from, $eur->rate_to / $eur->rate_from, $eur->id);
	}

	$_SERVER['DOCUMENT_ROOT'];

	$fp = fopen('/home/c/cveti63ru/new.stroykomfort63.ru/public_html/cron/curs.txt', 'w');
	fwrite($fp, $CurentCursUSD.' '.$CurentCursEUR);
	fclose($fp);